from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
"""
Modules that saves all experiment constants
"""

MODELS = []
ENSEMBLES = []

"""
Available Scoring: 
‘accuracy’
‘balanced_accuracy’
‘average_precision’
‘brier_score_loss’
‘f1’
‘f1_micro’
‘f1_macro’
‘f1_weighted’
‘f1_samples’
‘neg_log_loss’
‘precision’
‘recall’    
‘roc_auc’
"""
SCORING = ['accuracy', 'roc_auc', 'f1']
NUM_FOLDS = 10
SEED = 7

MODELS.append(('LR', LogisticRegression(solver='lbfgs')))
MODELS.append(('LR balanced', LogisticRegression(solver='lbfgs',class_weight='balanced')))
MODELS.append(('LR balanced 2', LogisticRegression(solver='lbfgs',class_weight={0:0.01,1:0.99})))
MODELS.append(('LDA', LinearDiscriminantAnalysis()))
MODELS.append(('KNN', KNeighborsClassifier()))
MODELS.append(('CART', DecisionTreeClassifier()))
# MODELS.append(('NB', GaussianNB()))
# # MODELS.append(('SVM', SVC(gamma='scale')))
#
# ENSEMBLES.append(("CostBag", CostSensitiveBaggingClassifier()))
# ENSEMBLES.append(("CostLR", CostSensitiveLogisticRegression()))
# ENSEMBLES.append(('AB', AdaBoostClassifier()))
ENSEMBLES.append(('GBM', GradientBoostingClassifier(n_estimators=100)))
ENSEMBLES.append(('RF', RandomForestClassifier(n_estimators=100)))
ENSEMBLES.append(('ET', ExtraTreesClassifier(n_estimators=100)))
