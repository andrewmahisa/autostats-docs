from matplotlib import pyplot as plt
from sklearn.model_selection import StratifiedKFold
from experiments import constants
from postprocessing import metric
import numpy as np

models = constants.MODELS
ensembles = constants.ENSEMBLES
num_folds = constants.NUM_FOLDS
seed = constants.SEED
"""
Variable that keeps track of all the models that we might use
"""


def basic_models_summary(X, y, metrics=[]):
    """

    train all basic models (LR,LDA,KNN,CART,NB,SVM) for data x,y and summarize
    :param X: DataFrame features
    :param y: DataFrame/numpy ndarray labels
    :return: dict name of the model and its respective results

    """
    results = {}
    for name, model in models:
        kfold = StratifiedKFold(n_splits=num_folds, random_state=seed)
        cv_results = metric.get_metrics(model, X, y, metrics=metrics)
        results[name] = cv_results
    return results


def ensemble_models_summary(X, y, metrics=[]):
    """

    train all ensemble models (AB,GBM,RF,ET) for data x,y and summarize
    :param X: DataFrame features
    :param y: DataFrame/numpy ndarray labels
    :return: dict name of the model and its respective results

    """
    results = {}
    for name, model in ensembles:
        kfold = StratifiedKFold(n_splits=num_folds, random_state=seed)
        cv_results = metric.get_metrics(model, X, y, metrics=metrics)
        results[name] = cv_results
    return results


def get_aggregate_summary(X, y, metrics=[], main_eval_metric=[]):
    """

    get summary for all models and plot it for analysis
    :param X: DataFrame features
    :param y: DataFrame/numpy ndarray labels
    :param metrics: List list of metric to use
    :param main_eval_metric: string main evaluation metric that will be used in comparison plot
    :return:
        dict to dict, respective results, with keys being  model name, and the result for that model

    """
    results = {}
    basic_results = basic_models_summary(X, y, metrics)
    ensemble_results = ensemble_models_summary(X, y, metrics)
    results.update(basic_results)
    results.update(ensemble_results)
    try:
        for key, cv_results in results.items():
            print(key, np.mean(cv_results['test_%s' % main_eval_metric]))
    except KeyError:
        raise ValueError("Main evaluation metric hasn't been calculated yet")
    return results
