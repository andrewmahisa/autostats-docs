from eli5 import show_prediction
from eli5 import show_weights


def get_explanation(model, features=None):
    show_weights(model)
