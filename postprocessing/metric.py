from matplotlib import gridspec
from sklearn.model_selection import cross_validate
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score

from postprocessing import cross_validation

from scipy import interp
import math
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import warnings
import itertools


def _plot_approval_bad_rate_curve(ax, model, X, y, threshold=0.5):
    """

    Plot the approval-bad-rate curve tradeoff for given prediction

    :param ax: matplotlib axes, axes that we will draw the plot into
    :param model: model that we want to plot
    :param X: array-like, features
    :param y: array-like, labels
    :return:

    """
    results, labels = cross_validation.cross_validate(model, X, y, strategy='KFold')
    for result, label in zip(results, labels):
        range = (0, 1)   # someday we can utilize this to develop this metric for score (which has different range)
        total_bad_rate, total_approval_rate = _approval_bad_rate(label, result, range, 100)
        ax.plot(total_approval_rate, total_bad_rate, color='darkorange', lw=1)
    ax.set_xlabel('Total Approval Rate %')
    ax.set_ylabel('Total Bad Rate %')
    ax.set_title('Bad Rate- Total Approval Rate Tradeoff')


def _plot_probability_dist_curve(ax, model, X, y, threshold=0.5):
    """

    Plot Distribution of predicted probability of our model.

    :param ax: matplotlib axes, axes that we will draw the plot into
    :param model: model that we want to plot
    :param X: array-like, features
    :param y: array-like, labels
    :param threshold: float, Treshold, ignored, for compatibility purposes
    :return:

    """
    results, labels = cross_validation.cross_validate(model, X, y, strategy='Standard')
    ax.hist(results[0])
    ax.set_xlabel('Probability %')
    ax.set_ylabel('Frequency %')
    ax.set_title('Distribution of Probability')


def _plot_roc_curve(ax, model, X, y, threshold=0.5):
    """

    Plot ROC(receiver operating characteristic) curve for given model and dataset

    :param ax: matplotlib axes, axes that we will draw the plot into
    :param model: model that we want to plot
    :param X: array-like, features
    :param y: array-like, labels
    :param threshold: float, Threshold, ignored, for compatibility purposes
    :return:

    """

    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    i = 1
    results, labels = cross_validation.cross_validate(model, X, y, strategy='KFold')
    for result, label in zip(results, labels):
        fpr, tpr, t = roc_curve(label, result)
        tprs.append(interp(mean_fpr, fpr, tpr))
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        ax.plot(fpr, tpr, lw=2, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i = i + 1
    ax.plot([0, 1], [0, 1], linestyle='--')
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    ax.set_title('ROC')


def _plot_confusion_matrix(ax, model, X, y, threshold=0.5):
    """

     Confusion Matrix for the given model and threshold

     :param ax: matplotlib axes, axes that we will draw the plot into
     :param model: model that we want to plot
     :param X: array-like, features
     :param y: array-like, labels
     :param threshold: float, Threshold, threshold for transforming probability
            into prediction -> if proba < threshold -> 0, if proba >= threshold -> 1
     :return:

     """
    results, labels = cross_validation.cross_validate(model, X, y, strategy='Standard')
    y_pred = [1 if x >= threshold else 0 for x in results[0]]
    cm = confusion_matrix(labels[0], y_pred)
    ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.set_title('Confusion Matrix')
    classes = ['GOOD', 'BAD']
    tick_marks = np.arange(len(classes))
    ax.tick_params(axis='x', rotation=45)
    ax.set_xticks(tick_marks)
    ax.set_yticks(tick_marks)
    ax.set_xticklabels(classes)
    ax.set_yticklabels(classes)

    fmt = '.2f'
    tres = cm.max() /2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        ax.text(j, i, format(cm[i, j], fmt),
                   horizontalalignment="center",
                  color="white" if cm[i, j] > tres else "black")

    ax.set_ylabel('True label')
    ax.set_xlabel('Predicted label')

def _approval_bad_rate(y, y_prob, bin_range, bins):
    """

    Calculate bad rate and approval rate for given prediction (y_prob) and labels (y)

    :param y: array-like, labels
    :param y_prob: array-like, probability of bad in model (.predict_proba() )
    :param bin_range: python range, range of the bins, should be (0,1) if we're using probability or (min_score,max_score) if we're using score
    :param bins: integer, how many bins should we separate the result into
    :return: total_bad_rate: array, total bad rate for every bins
             total_approval_rate: array, total bad rate of every bins

    """
    df = pd.DataFrame({'Labels': y, 'Probability': y_prob})
    prob_bad = df.loc[df['Labels'] == 1, ['Probability']]
    bin_bad, edges_bad = np.histogram(prob_bad, bins=bins, range=bin_range)       # Put the bad data into histogram intervals based on probability threshold
    bin_total, edges_total = np.histogram(y_prob, bins=bins, range=bin_range)   # Put the total data into histogram intervals based on probability threshold
    total_bad_rate = np.cumsum(bin_bad/np.sum(bin_total))*100
    total_approval_rate = np.cumsum(bin_total/np.sum(bin_total))*100
    return total_bad_rate, total_approval_rate


def _metric_accuracy(y, y_proba, threshold=0.5):
    """
    Calculate standard accuracy metric : True Positives / Size of Sample
    :param y: List labels or ground truth or flag
    :param y_proba: List prediction result (in probability form)
    :param threshold: float, Threshold, threshold for transforming probability
            into prediction -> if proba < threshold -> 0, if proba >= threshold -> 1
    :return:
    """
    y_pred = [1 if x >= threshold else 0 for x in y_proba]
    return accuracy_score(y_true=y, y_pred=y_pred)


def _metric_roc_auc(y, y_proba, threshold=0.5):
    """
    Calculate roc_auc, area under ROC curve.
    :param y: List labels or ground truth or flag
    :param y_proba: List prediction result (in probability form)
    :param threshold: float, ignored, for compatibility
    :return:
    """
    return roc_auc_score(y_true=y, y_score=y_proba)


def _metric_gini(y, y_proba, threshold=0.5):
    """
    Calculate GINI statistic, computed as 2 * auc - 1
    :param y: List labels or ground truth or flag
    :param y_proba: List prediction result (in probability form)
    :param threshold: float, ignored, for compatibility
    :return:
    """
    return 2*roc_auc_score(y_true=y,y_score=y_proba)-1


def _metric_precision(y, y_proba, threshold=0.5):
    """
    Calculate precision metric = True Positive / (True Positive + False Positive )
    :param y: List labels or ground truth or flag
    :param y_proba: List prediction result (in probability form)
    :param threshold: float, Threshold, threshold for transforming probability
            into prediction -> if proba < threshold -> 0, if proba >= threshold -> 1
    :return:
    """
    y_pred = [1 if x >= threshold else 0 for x in y_proba]
    return precision_score(y_true=y, y_pred=y_pred)


def _metric_recall(y, y_proba, threshold=0.5):
    """
    Calculate recall metric = True Positive / (True Positive + False Negative )
    :param y: List labels or ground truth or flag
    :param y_proba: List prediction result (in probability form)
    :param threshold: float, Threshold, threshold for transforming probability
            into prediction -> if proba < threshold -> 0, if proba >= threshold -> 1
    :return:
    """
    y_pred = [1 if x >= threshold else 0 for x in y_proba]
    return recall_score(y_true=y, y_pred=y_pred)


def get_metrics(model, X, y, metrics=[]):
    """

    get statistics / metrics for given model and dataset
    Currently Available Metrics are :

    *accuracy* : Standard Statistical Accuracy

    *balanced_accuracy* : Balanced accuracy (sum of recall for each class divided by the number of class)

    *f1* : Standard F1 Metric

    *precision* : Precision metric

    *recall* : Recall Metric

    *roc_auc* : Area under ROC (Receiver Operating Characteristic Curve)

    *gini* : Gini coefficient / normalized gini for this model on test dataset

    :param model: Statistical Model, can be anything as long as it implements .predict_proba() and predict()
    :param X: numpy ndarray, array of features and its values
    :param y: numpy ndarray, array of labels
    :param metrics: list of string, List of metrics that we want to calculate
    :return: **score** : dict, dictionary where the metric strings are the keys. ex: score['test_roc_auc'] holds values for roc_auc metric on test dataset

    """
    warnings.warn("Please use get_callable_metrics() instead",DeprecationWarning)
    sklearn_available_metric = {'accuracy', 'balanced_accuracy', 'average_precision',
                                    'brier_score_loss', 'f1', 'f1_micro', 'f1_macro', 'f1_weighted',
                                    'f1_samples', 'neg_log_loss', 'precision', 'recall', 'roc_auc'}

    # Filter metric based on the ones available on sklearn, and the one we make ourself
    sklearn_filtered_metric = [x for x in metrics if x in sklearn_available_metric]
    manual_metric = [x for x in metrics if x not in sklearn_available_metric]
    score = dict()
    cv = StratifiedKFold(n_splits=10, random_state=999)

    # use sklearn for the rest
    if sklearn_filtered_metric:
        sklearn_score = cross_validate(model, X, y, cv=cv, scoring=sklearn_filtered_metric)
        score.update(sklearn_score)

    # for everything that we made ourself, calculate
    for metric in manual_metric:
        if metric == 'gini':
            try:
                score['test_gini'] = score['test_roc_auc'] * 2 - 1
                score['train_gini'] = score['train_roc_auc'] * 2 - 1
            except KeyError:
                # key error = we haven't computed roc_auc yet
                score['test_roc_auc'] = cross_val_score(model, X=X, y=y, scoring='roc_auc', cv=cv)
                score['test_gini'] = score['test_roc_auc'] * 2 - 1
        else:
            warnings.warn("Metrics %s not found or not yet implemented, ignoring..." % metric)
    return score


def get_callable_metrics(y, y_proba, treshold=0.5, metrics=[]):
    """

    Get metrics for given cross-validation results.

    :param y: List of list, the ground-truth/classes for each cross-validation fold/split
    :param y_proba: List of list, the predicted probability for each cross-validation fold/split
    :param treshold: float the probability threshold used to define which predictions are considered 0 and 1.
            if proba < threshold then 0, otherwise 1.
    :param metrics: List the metrics that we want to calculate from the given cv-results.
    :return: **score**: Dictionary, each metric is associated with its name as a key for the dictionary
            for example: score['accuracy'] gives us the prediction accuracy

    """
    available_metrics = {
        'accuracy': _metric_accuracy,
        'roc_auc': _metric_roc_auc,
        'gini': _metric_gini,
        'recall': _metric_recall,
        'precision': _metric_precision
    }
    results = {}
    for metric in metrics:
        fold_result = []
        for result, label in zip(y_proba, y):
            fold_result.append(available_metrics[metric](label, result, treshold))
        results[metric] = fold_result
    return results


def get_plots(model, X, y, plots=[], threshold=0.5):
    """

    show all requested plot in 1 figure

    :param model: model that we want to plot
    :param X: arraylike features
    :param y: arraylike labels
    :param plots: List of string, name of the plots that we want to plot
    :return:

    """
    available_plots = {
        'batc': _plot_approval_bad_rate_curve,
        'roc': _plot_roc_curve,
        'proba_dist': _plot_probability_dist_curve,
        'confusion_matrix': _plot_confusion_matrix
    }
    max_plot_in_column = 3
    cols = len(plots) if len(plots) < max_plot_in_column else max_plot_in_column
    # column = sequence of 1 2 3 1 2 3 1 2 3
    # cols = (len(plots)-1) % (max_plot_in_column) + 1
    # rows = basically every 3 columns go next
    rows = int(math.ceil(len(plots)/max_plot_in_column))
    fig = plt.figure()
    fig.set_size_inches(1280/96, 720/96) #720p
    gs = gridspec.GridSpec(rows, cols)

    for index, plotname in enumerate(plots):
        ax = fig.add_subplot(gs[index])
        available_plots[plotname](ax, model, X, y, threshold)
    fig.tight_layout()
    plt.show()
