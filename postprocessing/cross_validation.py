from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from experiments import constants
import pandas as pd
import numpy as np


def _kfold_split(model, X, y, n_splits=5):
    """

    Perform cross validation (with model fitting) with standard k-fold
    (split dataset into k-parts, for each parts,train with other k-1 parts, and test with that part)

    :param model: the model to perform cross-validation on
    :param X: Arraylike features
    :param y: Arraylike labels
    :param n_splits: Integer how many splits that we want
    :return: cv_results: List, probability prediction on the test set, each elements in the list represents the test
                                result for a single field (which is also a list)
             cv_labels: List, the labels/ground truth for experiments

    """

    cv = KFold(n_splits=n_splits, random_state=constants.SEED)
    cv_results = []
    cv_labels = []
    for train_index, test_index in cv.split(X, y):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        model.fit(X_train,y_train)
        cv_results.append(model.predict_proba(X_test)[:, 1])
        cv_labels.append(y_test.values.ravel())
    return cv_results, cv_labels


def _stratified_split(model, X, y, n_splits=5):
    """

    Perform cross validation (with model fitting) with stratified k-fold
    (a modified k-fold where the class percentage on each fold is maintained)

    :param model: the model to perform cross-validation on
    :param X: Arraylike features
    :param y: Arraylike labels
    :param n_splits: Integer how many splits that we want
    :return: cv_results: List, probability prediction on the test set, each elements in the list represents the test
                                result for a single field (which is also a list)
             cv_labels: List, the labels/ground truth for experiments
    """
    cv = StratifiedKFold(n_splits=n_splits, random_state=constants.SEED)
    cv_results = []
    cv_labels = []
    for train_index, test_index in cv.split(X, y):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        model.fit(X_train, y_train)
        cv_results.append(model.predict_proba(X_test)[:, 1])
        cv_labels.append(y_test.values.ravel())
    return cv_results, cv_labels


def _sorted_split(model, X, y, sort_field, test_size=0.2):
    """
    
    Perform a train-test split with the data sorted according to the sort field. This allow us to get test set
    that is feature specific. (ex: split the data according to last month, or lowest application to payment ratio)
    
    :param model: the model to perform cross-validation on
    :param X: Arraylike features
    :param y: Arraylike labels
    :param sort_field : String field that we use as a reference
    :param test_size : float percentage of data that should be on the test set
    :return: cv_results: List, probability prediction on the test set, each elements in the list represents the test
                                result for a single field (which is also a list)
             cv_labels: List, the labels/ground truth for experiments
    """

    cv_results = []
    cv_labels = []
    try:
        selected_columns = list(X.columns)
        selected_columns.remove(sort_field)
        full_dataset = pd.concat([X, y], axis=1)
        full_dataset.sort_values(by=[sort_field], inplace=True)
        test_dataset = full_dataset.tail(int(len(full_dataset)*test_size))
        train_dataset = full_dataset.head(int(len(full_dataset)*(1-test_size)))
        X_train = train_dataset[selected_columns]
        y_train = train_dataset.iloc[:, -1].values.ravel()
        X_test = test_dataset[selected_columns]
        y_test = test_dataset.iloc[:, -1].values.ravel()
        model.fit(X_train, y_train)
        cv_results.append(model.predict_proba(X_test)[:, 1])
        cv_labels.append(y_test)
    except ValueError as error:
        raise ValueError(error)
    return cv_results, cv_labels


def _standard_split(model, X, y, test_size=0.2):
    """

    Perform a standard train-test split (just separating based on percentage)

    :param model: the model to perform cross-validation on
    :param X: Arraylike features
    :param y: Arraylike labels
    :param test_size : float percentage of data that should be on the test set
    :return: cv_results: List, probability prediction on the test set, each elements in the list represents the test
                                result for a single field (which is also a list)
             cv_labels: List, the labels/ground truth for experiments
    """
    try:
        cv_results = []
        cv_labels = []
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size,
                                                            random_state=constants.SEED)
        model.fit(X_train, y_train)
        cv_results.append(model.predict_proba(X_test)[:, 1])
        cv_labels.append(y_test)
    except ValueError as error:
        raise ValueError(error)
    return cv_results, cv_labels


def cross_validate(model, X, y, strategy='Stratified', sort_field='approvedDate', n_splits=5, test_size=0.2):
    """

    Perform cross validation on given model and dataset with specified parameters and strategy

    :param model: the model to perform cross-validation on
    :param X: Dataframe features
    :param y: Dataframe labels, ground-truth, classes, or flags.
    :param strategy: str either Stratified, Standard, KFold, or Sorted, strategy that we want to use
    :param sort_field: str field that we use as a reference
    :param n_splits: int the amount of splits (K) used in K-Fold and Stratified cross-validation
    :param test_size: float the percentage of dataset that should be used for test
    :return: **results**: List, probability prediction on the test set, each elements in the list represents the \
            test result for a single field (which is also a list)

            **labels**: List, the labels/ground truth for experiments

    """
    results = []

    labels = []
    try:
        date_data = X.select_dtypes(include=[np.datetime64])
        if strategy != 'Sorted':
            if not date_data.empty:
                X.drop(date_data, inplace=True, axis=1)

        if strategy == 'Stratified':
            results, labels = _stratified_split(model, X, y, n_splits=n_splits)
        elif strategy == 'KFold':
            results, labels = _kfold_split(model, X, y, n_splits=n_splits)
        elif strategy == 'Standard':
            results, labels = _standard_split(model, X, y, test_size=test_size)     
        elif strategy == 'Sorted':
            results, labels = _sorted_split(model, X, y, sort_field, test_size=test_size)
    except KeyError:
        raise KeyError("Please make sure the sortfield is on the column list if you're using Centile Split")
    return results, labels
