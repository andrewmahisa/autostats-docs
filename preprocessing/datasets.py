import pandas as pd
import warnings
import datetime
import dateutil.parser


def import_dataset(file, sheet=None, delimiter=',', column_range=None, label_name=None, drop_na=False):
    """

    import dataset from file, automatically handles both csv or excel file

    :param file: str file to load from
    :param sheet: str sheetname to load from (for excel files)
    :param column_range: str column to parse, using standard excel format "A:Z"
    :param delimiter: str delimiter used in dataset
    :param label_name: str the column name for the labels/ground-truth/flags
    :param drop_na: boolean whether to drop null values on load
    :return: X, y : Arraylike, the loaded data, splitted into features(X) and labels/flags (y) if label name is specified
            df : Dataframe, the loaded data if label name is not specified

    """
    df = pd.DataFrame()
    if file.lower().endswith('.csv'):
        if sheet is not None:
            warnings.warn("Specifying sheet for a csv file, Ignoring..")
        df = pd.read_csv(file, sep=delimiter)
    elif file.lower().endswith(('.xls', '.xlsx')):
        df = pd.read_excel(file, sheet_name=sheet, usecols=column_range)

    if drop_na is True:
        df.dropna(inplace=True)

    if label_name is not None:
        y = df[label_name]
        X = df.drop([label_name], axis=1)
        return X, y
    return df


def filter_by_date(data, start_date, end_date, field_name='approvedDate'):
    """

    Filter our dataset based on date, helps with checking and validation, or further analytics
    :param data: Dataframe, dataset to filter
    :param start_date: str String that represents start date ( can follow almost all standard date formats )
    :param end_date: str String that represents end date ( can follow almost all standard date formats )
    :param field_name: str the name of the date column that we use as a pivot for filtering
    :return: filtered_data, unfiltered data : data that gets filtered by the input date, and data that doesn't (for validation purposes)

    """
    start=dateutil.parser.parse(start_date)
    end=dateutil.parser.parse(end_date)
    filtered_data = data[(data[field_name]>start) & (data[field_name]<end)]
    unfiltered_data = data[~((data[field_name]>start) & (data[field_name]<end))]
    return filtered_data, unfiltered_data
