import pandas as pd
import numpy as np
from sklearn import preprocessing


def scale_variables(data, method="minmaxscaler", columns=None):
    """

    Scale the dataset according to the minmax (0-1 Scaling),standard scaler (0 Mean 1-Std Deviation Scaling),
    robust scaler (quartile-median scaling)

    :param data: Dataframe data used in the dataframe
    :param method: string either "minmaxscaler" or "stdscaler" or "robustscaler", method to use
    :param columns: list List of columns to scale
    :return: Dataframe, data where all the values in specified columns are scaled using selected method

    """

    # making sure the passed dataframe isn't modified, for all assignment operations, we'll use temporary dataframe
    data_copy = data.copy()

    if method == "stdscaler":
        if columns is None:
            scaler = preprocessing.StandardScaler()
            numerical_data = data_copy.select_dtypes(include=[np.number])
            scaled_df = scaler.fit_transform(numerical_data)
            data_copy[numerical_data.columns] = pd.DataFrame(scaled_df, columns=numerical_data.columns)
            return data_copy
        else:
            scaler = preprocessing.StandardScaler()
            unscaled_features = data_copy[columns]
            scaled_features = scaler.fit_transform(unscaled_features)
            scaled_features = pd.DataFrame(scaled_features, columns=unscaled_features.columns)
            data_copy[columns] = scaled_features
            return data_copy

    elif method == "minmaxscaler":
        # if no columns are specified, scale everything
        if columns is None:
            return pd.DataFrame(preprocessing.minmax_scale(data), columns=data.columns)
        # otherwise just scale the specified columns
        else:
            data_copy[columns] = preprocessing.minmax_scale(X=data_copy[columns])
            return data_copy

    elif method == "robustscaler":
        if columns is None:
            scaler = preprocessing.RobustScaler()
            numerical_data = data_copy.select_dtypes(include=[np.number])
            scaled_df = scaler.fit_transform(numerical_data)
            data_copy[numerical_data.columns] = pd.DataFrame(scaled_df, columns=numerical_data.columns)
            return data_copy
        else:
            scaler = preprocessing.RobustScaler()
            unscaled_features = data_copy[columns]
            scaled_features = scaler.fit_transform(unscaled_features)
            scaled_features = pd.DataFrame(scaled_features, columns=unscaled_features.columns)
            data_copy[columns] = scaled_features
            return data_copy


