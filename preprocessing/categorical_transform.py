import pandas as pd
import numpy as np
import category_encoders as ce
from sklearn import preprocessing


def encode_variables(data, encoding="onehot", columns=None):
    """

    encode categorical variables with onehot (dummy) Encoding or
    label (assigning Integers into each categorical class) encoding

    :param data: Dataframe to encode
    :param encoding: Type of encoding to use, either "onehot" for onehot encoding or "label" for label encoding
    :param columns: Columns to encode, if None encode all categorical variables
    :return: Dataframe, encoded data

    """
    # making sure the passed dataframe isn't modified, for all assignment operations, we'll use temporary dataframe
    data_copy = data.copy()
    if encoding == "onehot":
        # if no column is specified, encode everything that is categorical
        if columns is None:
            return pd.get_dummies(data)
        else:
            return pd.get_dummies(data, columns=columns)
    elif encoding == "label":
        # if no column is specified, encode everything that is categorical
        labelencoder = preprocessing.LabelEncoder()
        if columns is None:
            categorical_data = data.select_dtypes(exclude=[np.number, np.datetime64]).apply(labelencoder.fit_transform)
            numerical_data = data.select_dtypes(include=[np.number, np.datetime64])
            if categorical_data.empty:
                return numerical_data
            else:
                return pd.concat([numerical_data, categorical_data], axis=1)
        else:
            data_copy[columns] = data_copy[columns].apply(labelencoder.fit_transform)
            return data_copy
