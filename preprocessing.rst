Preprocessing Module
=====================================

Numerical Preprocessing
-------------------------------------

This module handles preprocessing for numerical variables, which include:

    #. Min - Max Scaling : scales all numeric variables in the range [0,1]. One possible formula is given below
        .. math::
                X_{new} = \frac{X-X_{min}}{X_{min}-X_{max}}
    #. Standard Scaling (Z-Score Scaling) : removes mean and scale the data to have unit variance
        .. math::
                X_{new} = \frac{X-\mu}{\sigma}
    #. Robust Scaling (Median-IQR Scaling): removes median and scale the data to interquartile range
        .. math::
                X_{new} = \frac{X-Q_2}{Q_3-Q_1}
        .. math::
             where \ Q_x \ indicates \ the \ x^{th} \ quartile

.. automodule:: preprocessing.numerical_transform
   :members:

Categorical Preprocessing
-------------------------------------
This module handles preprocessing for numerical variables, which for now includes encoding categorical variables into numerical, so that it's ready to use for machine-learning models

    Sample Data:

    .. image:: images/base.png

This Module Supports the following encoding technique:

    #. **Label Encoding:** Encode labels with value between 0 and n_classes-1.

        .. image:: images/label.png

    #. **One Hot Encoding:** Encode categorical integer features as a one-hot numeric array.

        .. image:: images/onehot.png


.. automodule:: preprocessing.categorical_transform
   :members: