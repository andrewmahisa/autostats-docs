.. Autostats documentation master file, created by
   sphinx-quickstart on Thu Jan  3 16:23:13 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Autostats's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   preprocessing.rst
   cross-validation.rst
   metric.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
