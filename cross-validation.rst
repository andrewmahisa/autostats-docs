Cross Validation Module
===============================
As we all know, learning the parameters of a prediction function and testing it on the same data is a methodological mistake: a model that would just repeat the labels of the samples that it has just seen would have a perfect score but would fail to predict anything useful on yet-unseen data.This situation is called **overfitting**.

To avoid it, it is common practice when performing a (supervised) machine learning experiment \
to split the dataset into few parts, and train the model only on specific part of the dataset, while keeping the testing set away from the training process.
This technique is generally called **'Cross-Validation'**.

This module aim to automate the process of **cross-validation**. Making the process of
        #. Splitting the dataset
        #. Training on *'train'* of the dataset
        #. Testing results on the *'test'* part of the dataset

There are several techniques to split the dataset, and this module supports the following technique:
        #. Standard Train-Test Split : Splitting the dataset into train and test sets, based on the size of the data

            Example : 70-30 Train Test Split means that we have 70% data as train, other 30% as test

        #. Sorted Train-Test Split: Splitting the dataset into train and test sets, based on the size of the data, while sorting the dataset according to a field

            Example : 70-30 Sorted Train Test Split on Date means that we have last 30% data (according to date) as test, and 70% data (according to date as train)

        #. K-Fold Cross Validation : Divides all the samples in *k* groups of samples, called folds, of equal sizes (if possible). The prediction function is learned using folds, and the fold left out is used for test.

            Example :
                        .. image:: images/kfold.png

        #. Stratified K-Fold Cross Validation : Basically The same as K-Fold, however on each fold, the % of members in each class are conserved.


            Example : if the classes for our training dataset is 80:20 for class 'Good':'Bad', then on each fold, there will be 80:20 'Good:Bad' Ratio
                        .. image:: images/stratified.png

.. automodule:: postprocessing.cross_validation
    :members:
